# Fun Digits

## Solution Discussion

The problem is splitted into those subjects to consider: 

A. Sorting by api. (time O(NlogN)), or , considering input data values restriction, when using counting sort, time O(N). Both scenarios space O(N)

B. Distributing digits to make counts in each bucket even (or close to even).

C. Calculate sum with ${sum of digit in bucket} * designated decimal

D. (Optional) Displaying Combined by digits from each bucket. (time O((N/{m of numbers from N})!), space O(N))

While I start exploring solutions, I did not see the necessarity of restrict number count to 2 if considering max the sum is to group those digits and assign bigger digits to high order decimal position.

The [EvenCountsTests](java/EvenCountsTests.java) is my initial solutions. 


Considering all the possible combination of digits representing number, I later on developed [EvenDistributesDemoCombTests](java/EvenDistributesDemoCombTests.java). Besides, digits grouping is through range chop, comparing to curor moving in the [EvenCountsTests](java/EvenCountsTests.java)

Scala solution is just to get max sum result. The Second solution grouping by identity(_) kind of reflecting the counting sort approach.


## How to run code

I put java solution to exec under test framework so that each scenarios can be executed separately. Those should be executed through IDE when copy paste

I post a REPL snapshot under scala

