import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class EvenCountsTests {

    /**
     *
     * @param d Given collection of input digit 0-9s
     * @param numberCount number of groups to distribute
     * @return sum of group numbers each with max value representation
     */
    public int funWDigitSum(int[] d, int numberCount) {
        Arrays.sort(d); // Calling API W. O(N) Nlog(N)
        int[] rs = new int[numberCount];

        int numIdx = 0;
        for(int i =d.length -1 ; i >=0; i--) {
            rs[numIdx] = rs[numIdx] * 10 + d[i];
            numIdx = (numIdx + 1) % numberCount;
        }

        int r = Arrays.stream(rs).sum();
        return r;
    }

    @Test
    public void testEvenCountsWMaxSum101() {
        int[] d = {9, 4, 2, 7, 9, 0};
        int numberCount = 2;
        int r = funWDigitSum(d, numberCount);
        System.out.println(r);
    }

    @Test
    public void testEvenCountsWMaxSum102() {
        int[] d = {9, 4, 2, 7, 9, 0, 1};
        int numberCount = 2;
        int r = funWDigitSum(d, numberCount);
        System.out.println(r);
    }

    @Test
    public void testEvenCountsWMaxSum103() {
        int[] d = {9, 4, 2, 7, 9, 0, 1};
        int numberCount = 2;
        int r = funWDigitSum(d, numberCount);
        System.out.println(r);
    }

    @Test
    public void testEvenCountsWMaxSum104() {
        int[] d = {3, 2, 1, 3, 2, 1, 3, 2, 1}; //TODO: input order pattern, if considered, could be used to improve sort O(N)
        int numberCount = 2;
        int r = funWDigitSum(d, numberCount);
        System.out.println(r);
    }

    @Test
    public void testEvenCountsWMaxSum() {
        int[] d = {1};
        int numberCount = 2;
        int r = funWDigitSum(d, numberCount);
        System.out.println(r);
    }

    @Test
    public void testEvenCountsWMaxSum201() {
        int[] d = {1,2,3,1,2,3,4};
        int numberCount = 3;
        int r = funWDigitSum(d, numberCount);
        System.out.println(r);
    }

    @Test
    public void testEvenCountsWMaxSum202() {
        int[] d = {3, 6, 4, 9, 0, 1};
        int numberCount = 4;
        int r = funWDigitSum(d, numberCount);
        System.out.println(r);
    }


}
