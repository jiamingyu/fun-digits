import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;

public class EvenDistributesDemoCombTests {

    public int funWDigitSumNDigitsCombIntuitive(int[] d, int numberCount, boolean showNumbers) {
//        Arrays.sort(d); // Calling API W. Time O( NlogN )
        // Since the problem is for digit represents 0-9, we can apply counting sort to achieve better time O()
        countingSort(d); // Time  O(N), space O(N)

        Stack<int[]> digitGroupStack = new Stack<>();

        int headIdx = 0;
        Integer sumRtn = 0;
        int decimalIncrements = 1;

        while(headIdx < d.length) {
            // chop range, leaving bigger values falling into possibly less count group (Odd total counts) guarantee their high order decimal position
            int[] ints = Arrays.copyOfRange(d, headIdx, headIdx + numberCount);
            digitGroupStack.add(ints);
            headIdx += headIdx + numberCount < d.length ? numberCount : d.length - headIdx;

            sumRtn = sumRtn  + Arrays.stream(ints).reduce(0, (a,b) -> a+b) * decimalIncrements;
            decimalIncrements *= 10;

        }

        if (showNumbers)
            displayNumbers(numberCount, digitGroupStack);

        return  sumRtn;
    }

    public void countingSort(int[] d) {

        int[] idxValCount = new int[10]; //constraint: here b[i] value must < b.length

        for (int i = 0; i < d.length; i++) {
            idxValCount[d[i]] += 1;
        }

        int fillCur = 0;
        for (int i =  0; i < idxValCount.length ; i++) {
            if (idxValCount[i] == 0)
                continue;

            for (int j = fillCur; j < fillCur + idxValCount[i]; j++)
                d[j] = i;

            fillCur += idxValCount[i];
        }

    }

    //O((N/{numbers count to generate})
    private void displayNumbers(int numberCount, Stack<int[]> digitGroupStack) {

        int[] nc = new int[numberCount];
        Arrays.setAll(nc, x -> x);

        ArrayList<int[]> idxOrderPermutes = new ArrayList<>();
        genIdxOrdersPermutation(nc, 0, idxOrderPermutes);

        Queue<List<Integer>> numbersBrewingQ = new LinkedList<>();
        List<Integer> largestDigits = Arrays.stream(digitGroupStack.pop()).boxed().collect(Collectors.toList());
        numbersBrewingQ.add(largestDigits);
        numbersBrewingQ.add(new ArrayList()); // Use empty col as marker in q

        while(!digitGroupStack.isEmpty()) {
            int[] digits = digitGroupStack.pop();

            while(!numbersBrewingQ.peek().isEmpty()) {
                List<Integer> numberLeads = numbersBrewingQ.poll();

                for(int[] idxOrder : idxOrderPermutes) {
                    // Each possible index order comb helps next digits[] to derive new numberlead W. +1
                    List<Integer> numbersLeadsToadd = new ArrayList<>();
                    for(int k = 0; k < numberCount; k++) {
                        int comb = numberLeads.get(k) * 10 + digits[idxOrder[k]];
                        numbersLeadsToadd.add(comb);
                    }
                    numbersBrewingQ.add(numbersLeadsToadd);
                }
            }
            numbersBrewingQ.add(numbersBrewingQ.poll()); // reset marker
        }

        while(!numbersBrewingQ.isEmpty() && !numbersBrewingQ.peek().isEmpty()) {
            String strs = numbersBrewingQ.poll().stream().map(String::valueOf).collect(Collectors.joining(","));
            System.out.println("combine rep: " + strs);
        }
    }

    private void genIdxOrdersPermutation(int[] d, int i, List<int[]> r) {
        if (i == d.length) {
            r.add(d.clone());
            return;
        }

        for (int j = i; j < d.length; j++) {
            swap(j, i, d);
            genIdxOrdersPermutation(d, j + 1, r);
            swap(j, i, d);
        }
    }

    private void swap(int j, int i, int[] d) {
        int bf = d[j];
        d[j] = d[i];
        d[i] = bf;
    }

    @Test
    public void testEvenCountsWMaxSum101() {
        int[] d = {9, 4, 2, 7, 9, 0};
//        int[] d = {9, 4, 2, 7, 8, 0};
        int numberCount = 2;
        int r = funWDigitSumNDigitsCombIntuitive(d, numberCount, true);
        System.out.println("sum of any of those comb : " + r);
    }

    @Test
    public void testEvenCountsWMaxSum101DisableShowingComb() {
        int[] d = {9, 4, 2, 7, 9, 0};
//        int[] d = {9, 4, 2, 7, 8, 0};
        int numberCount = 2;
        int r = funWDigitSumNDigitsCombIntuitive(d, numberCount, false);
        System.out.println("sum of any of those comb : " + r);
        assert r == 1912;
    }

    @Test
    public void testEvenCountsWMaxSum102() {
        int[] d = {9, 4, 2, 7, 9, 0, 1};
        int numberCount = 2;
        int r = funWDigitSumNDigitsCombIntuitive(d, numberCount, true);
        System.out.println("sum of any of those comb : " + r);
        assert r == 10661;
    }

    @Test
    public void testEvenCountsWMaxSum103() {
        int[] d = {9, 4, 2, 7, 9, 0, 1};
        int numberCount = 2;
        int r = funWDigitSumNDigitsCombIntuitive(d, numberCount, true);
        System.out.println(r);
        assert r == 10661;

    }

    @Test
    public void testEvenCountsWMaxSum104() {
        int[] d = {3, 2, 1, 3, 2, 1, 3, 2, 1}; //TODO: input order pattern, if considered, could be used to improve sort O(N)
        int numberCount = 2;
        int r = funWDigitSumNDigitsCombIntuitive(d, numberCount, true);
        System.out.println(r);
        assert r == 36432;

    }

    @Test
    public void testEvenCountsWMaxSum() {
        int[] d = {1};
        int numberCount = 2;
        int r = funWDigitSumNDigitsCombIntuitive(d, numberCount, true);
        System.out.println(r);
        assert r == 1;
    }

    @Test
    public void testEvenCountsWMaxSum201() {
        int[] d = {1,2,3,1,2,3,4};
        int numberCount = 3;
        int r = funWDigitSumNDigitsCombIntuitive(d, numberCount, true);

        System.out.println("sum of any of those comb : " + r);
        assert r == 484;
    }

    @Test
    public void testEvenCountsWMaxSum202() {
        int[] d = {3, 6, 4, 9, 0, 1};
        int numberCount = 4;
        int r = funWDigitSumNDigitsCombIntuitive(d, numberCount, true);
        System.out.println("sum of any of those comb : " + r);
        assert 158 == r;

    }

}
